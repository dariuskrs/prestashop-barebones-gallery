import babel from 'rollup-plugin-babel';
import { uglify } from 'rollup-plugin-uglify';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import scss from 'rollup-plugin-scss'

export default {
	input: './views/js/frontend-main.js',
	output: {
		file: './views/js/builds/bbgallery.min.js',
		format: 'iife',
		name: 'bundle'
	},
    plugins: [
        babel({
            exclude: 'node_modules/**'
		}),
		uglify(),
        resolve(),
		commonjs(),
		scss({
			output: './views/css/builds/bbgallery.min.css',
			//outputStyle: "compressed",
		})
    ]
}

