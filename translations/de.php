<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{barebonesgallery}prestashop>barebonesgallery_8b7333b25ead5e9f2accb581bd416db2'] = 'Barebones gallery';
$_MODULE['<{barebonesgallery}prestashop>barebonesgallery_e8fdb1aedbdf2891b2f2695e1aedba43'] = 'Bildergalerie für Prestashop';
$_MODULE['<{barebonesgallery}prestashop>barebonesgallery_2767cc3ede7592a47bd6657e3799565c'] = 'galerie';
$_MODULE['<{barebonesgallery}prestashop>barebonesgallery_819bbadcaffe077265f73154fb88a7a6'] = 'Anlegen der DB-Tabellen gescheitert';
$_MODULE['<{barebonesgallery}prestashop>barebonesgallery_1d6c59ef4c318383c1bf75d1dd716f62'] = 'Löschen der DB-Tabellen gescheitert';
$_MODULE['<{barebonesgallery}prestashop>adminbarebonesgallerymgmtgallery_490aa6e856ccf208a054389e47ce0d06'] = 'Id';
$_MODULE['<{barebonesgallery}prestashop>adminbarebonesgallerymgmtgallery_b78a3223503896721cca1303f776159b'] = 'Titel';
$_MODULE['<{barebonesgallery}prestashop>adminbarebonesgallerymgmtgallery_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Beschreibung';
$_MODULE['<{barebonesgallery}prestashop>adminbarebonesgallerymgmtgallery_d3b206d196cd6be3a2764c1fb90b200f'] = 'Auswahl löschen';
$_MODULE['<{barebonesgallery}prestashop>adminbarebonesgallerymgmtgallery_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Auswahl löschen?';
