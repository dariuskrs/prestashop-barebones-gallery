# Barebones gallery - A Prestashop module

A module for Prestashop to add Gallery page to prestashop.
The gallery comes with its own content type, which gets added to the admin.

## Prerequisites
Prestashop 1.7.6.x 

## Instructions (dev)
At the root of the project, and making sure you have a running node installation, run

```
npm install
npm run build
```

## Versions
- 0.2.0: first actual release
- 0.1.0: initial version; does the basics
