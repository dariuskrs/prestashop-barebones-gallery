<?php
include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('barebonesgallery.php');
require_once _PS_MODULE_DIR_.'/barebonesgallery/classes/BbImage.php';

if (Tools::getValue('action') == 'updateImagesPosition' && Tools::getValue('bbimage'))
{
	$images = Tools::getValue('bbimage');
	BbImage::updatePositions($images);
}