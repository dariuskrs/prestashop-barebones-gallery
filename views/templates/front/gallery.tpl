{extends file='page.tpl'}

{block name='page_title'}
  {$gallery.title}
{/block}

{block name='page_content_container'}
{if !empty($gallery.description)}
<p>{$gallery.description}</p>
{/if}

<section id="bbGallery">
{if !empty($imageList)}
  {foreach from=$imageList item=$image}
    {assign var="description" value="description_`$currentLanguageId`"}
  <a class="image-wrapper" href="{$image.thumbnails.full}" {if !empty($image["{$description}"])}data-caption="{$image["{$description}"]}"{/if}>
  <img src="{$image.thumbnails.medium}" alt="{if empty($image["{$description}"])}{$image.id_bbgallery_image}{else}{$image["{$description}"]}{/if}" class="image-thumb" loading="lazy" />
  </a>
  {/foreach}
{else}
  <p class="bbgallery-empty">{l s='This gallery does not contain any images for the time being.' mod='barebonesgallery'}</p>
{/if}
</section>

{*$gallery|var_dump*}
{*$imageList|var_dump*}

{literal}
<!--
  <script type="text/javascript">

  </script>
-->
{/literal}
{/block}
