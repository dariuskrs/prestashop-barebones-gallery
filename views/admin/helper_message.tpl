{if !empty($gallery_paths)}
<div class="alert alert-info" role="alert">
<p>{l s='You will find your galleries in their respective languages under' mod='barebonesgallery'}:
<ul>
{foreach from=$gallery_paths item=path}
    <li>{$path.lang}: {$path.url}/<{l s='slug field of your gallery' mod='barebonesgallery'}></li>
{/foreach}
</ul>
</p>
</div>
{/if}