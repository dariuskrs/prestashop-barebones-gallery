{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="panel"><h3><i class="icon-list-ul"></i> {l s='Image list' mod='barebonesgallery'}
	<span class="panel-heading-action">
		<a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminBarebonesGalleryMgmtGallery')}&configure=barebonesgallery&id_bbgallery_gallery={$id_bbgallery_gallery}&addimage=1">
			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Add new' mod='barebonesgallery'}" data-html="true">
				<i class="process-icon-new "></i>
			</span>
		</a>
	</span>
	</h3>
	<div id="galleryContent">
		<div id="bbgallery-images">
			{if empty($images)}
			<span class="empty-descr">{l s='No images for now. Please add some by hitting the "add" icon on the top right.' mod='barebonesgallery'}</span>
			{/if}
			{foreach from=$images item=img}
				<div id="bbimage_{$img.id_bbgallery_image}" class="panel">
					<div class="row">
						<div class="col-lg-1">
							<span><i class="icon-arrows "></i></span>
						</div>
						<div class="col-md-3">
						{if isset($img.thumbnails) && isset($img.thumbnails.small)}
							<img src="{$img.thumbnails.small}" alt="Image {$img.id_bbgallery_image}" loading="lazy" class="img-thumbnail" />
						{/if}
						</div>
						<div class="col-md-8">
							<h4 class="pull-left">
								#{$img.id_bbgallery_image}
							</h4>
							<div class="btn-group-action pull-right">
								{*$img.active*}

								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminBarebonesGalleryMgmtGallery')}&configure=barebonesgallery&id_bbgallery_gallery={$id_bbgallery_gallery}&id_bbgallery_image={$img.id_bbgallery_image}&updateimage=1">
									<i class="icon-edit"></i>
									{l s='Edit' mod='barebonesgallery'}
								</a>
								<a class="btn btn-default" onclick="if (!confirm('{l s='Remove the image?' mod='barebonesgallery'}')) return false;"
									href="{$link->getAdminLink('AdminBarebonesGalleryMgmtGallery')}&configure=barebonesgallery&id_bbgallery_gallery={$id_bbgallery_gallery}&id_bbgallery_image={$img.id_bbgallery_image}&deletebbgallery_image">
									<i class="icon-trash"></i>
									{l s='Delete' mod='barebonesgallery'}
								</a>
							</div>

							<div class="description" style="clear: both;">
								{foreach from=$languages item=language}
									<a href="#" class="language-selector {if $language.id_lang == $defaultLanguage}default-language active{/if}" 
										data-lang="{$language.id_lang}">{l s='Description' mod='barebonesgallery'} ({$language.iso_code})</a>
								{/foreach}
								{foreach from=$languages item=language}
								<div data-lang="{$language.id_lang}" 
								    class="detail {if $language.id_lang == $defaultLanguage}default-language active{/if} language-preview">
									{assign var="description" value="description_`$language.id_lang`"}
									{if empty($img["{$description}"])}
									<span class="empty-descr">{l s='No description has been set' mod='barebonesgallery'}</span>
									{else}
									{$img["{$description}"]}
									{/if}

								</div>
								{/foreach}
							</div>

						</div>
					</div>
				</div>
			{/foreach}
		</div>
	</div>
</div>
