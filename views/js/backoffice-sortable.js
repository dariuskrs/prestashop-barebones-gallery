$(function() {

    var $galleryElements = $("#bbgallery-images");
    $galleryElements.sortable({
        opacity: 0.6,
        cursor: "move",
        update: function() {
            let order = $(this).sortable("serialize") + "&action=updateImagesPosition";
            $.post('/modules/barebonesgallery/ajax_barebonesgallery.php', order);
        }
    });
    $galleryElements.hover(function() {
        $(this).css("cursor","move");
        },
        function() {
        $(this).css("cursor","auto");
    });

});