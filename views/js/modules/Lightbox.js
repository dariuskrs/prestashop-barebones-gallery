// import sayHello from './modules/Lightbox';
//import baguetteBox from 'baguettebox.js';
import baguetteBox from 'baguettebox.js';

const initGalleryLightbox = () => {
    //alert('test');
    baguetteBox.run('#bbGallery');
}

export default initGalleryLightbox;
