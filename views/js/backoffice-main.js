//import Sortable from 'sortablejs/modular/sortable.complete.esm.js';

;(function () {

    'use strict';

    function ready(callbackFunc) {
        if (document.readyState !== 'loading') {
          // Document is already ready, call the callback directly
          callbackFunc();
        } else if (document.addEventListener) {
          // All modern browsers to register DOMContentLoaded
          document.addEventListener('DOMContentLoaded', callbackFunc);
        } else {
          // Old IE browsers
          document.attachEvent('onreadystatechange', function() {
            if (document.readyState === 'complete') {
              callbackFunc();
            }
          });
        }
    }
    
    const activeElementClass = 'active';

    document.addEventListener('click', function (event) {

        // If the clicked element doesn't have the right selector, bail
        if (!event.target.closest('.language-selector')) return;
    
        // Don't follow the link
        event.preventDefault();

        let parent = event.target.parentNode;

        if (typeof parent !== "undefined") {

            [].forEach.call(parent.querySelectorAll('.description .detail'), function (el) {
                el.classList.remove(activeElementClass);
            });

            [].forEach.call(parent.querySelectorAll('.description .language-selector'), function (el) {
                el.classList.remove(activeElementClass);
            });

            let targetLanguage = event.target.dataset.lang;

            parent
                .querySelector(".detail[data-lang='"+ targetLanguage +"']")
                .classList
                .add(activeElementClass);

            event.target.classList.add(activeElementClass);
        }

    }, false);
    
})();
