<?php

class BbImage extends ObjectModel
{
    public $id_bbgallery_image;

    public $id_bbgallery_gallery = null;

    public $title;

    public $description;

    public $legend;

    public $url;

    public $image;

    public $position;

    public $active;

    public $date_add;

    public static $definition = [
      'table' => 'bbgallery_image',
      'primary' => 'id_bbgallery_image',
      'multilang' => true,
      'fields' => [
        'id_bbgallery_gallery' => ['type' => self::TYPE_INT, 'validate' => 'isunsignedInt'],
        'title' =>              ['type' => self::TYPE_STRING, 'validate' => 'isAnything', 'required'=> false, 'lang' => true],
        'description' =>        ['type' => self::TYPE_HTML, 'validate' => 'isAnything', 'lang' => true],
        'legend' =>             ['type' => self::TYPE_STRING, 'validate' => 'isAnything', 'lang' => true],
        'url' =>                ['type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true, 'required'=> false],
        'image' =>              ['type' => self::TYPE_STRING, 'validate' => 'isString','lang' => false, 'required'=> true],
        'position' =>           ['type' => self::TYPE_INT, 'validate' => 'isunsignedInt'],
        'active' =>             ['type' => self::TYPE_BOOL, 'validate' => 'isBool'],
        'date_add' =>           ['type' => self::TYPE_DATE, 'validate'  => 'isDate', 'copy_post' => false]
      ],
    ];

  private static $thumbnailTypes = array(
    'original'  => null,
    'full'      => 800,
    'medium'    => 400,
    'small'     => 350
  );

  private static $preferedThumbnailExtension = 'jpg';

    /**
     * 
     */
	public function __construct($id = null, $id_lang = null)
	{
        parent::__construct($id, $id_lang);
  }

  /**
   * 
   */
  public function setGalleryId($galleryId) 
  {
    $this->id_bbgallery_gallery = $galleryId;
  }

  /**
   * 
   */
  public function getGalleryId() {
    return $this->id_bbgallery_gallery;
  }

  /**
   * @param bool $excludeReturn
   * @return array|false|mysqli_result|null|PDOStatement|resource
   * @throws PrestaShopDatabaseException
   */
  public static function getImagesByGalleryId($galleryId, $alwaysRegenerateThumbnails = true, $disableCache = true)
  {
      if (empty($galleryId)) 
      {
        throw new Exception("Missing a gallery id");
      }

      $i18nSelect = array();
      $i18nJoin = '';
      foreach (Language::getLanguages(false) as $lang) {
        $i18nSelect[] = 'i18n_'.$lang['id_lang'].'.title as title_'.$lang['id_lang'];
        $i18nSelect[] = 'i18n_'.$lang['id_lang'].'.description as description_'.$lang['id_lang'];
        $i18nSelect[] = 'i18n_'.$lang['id_lang'].'.legend as legend_'.$lang['id_lang'];
        $i18nSelect[] = 'i18n_'.$lang['id_lang'].'.url as url_'.$lang['id_lang'];
      }

      // Select all the images for the all languages, one array per language
      $defaultLanguage = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
      $dbQuery = new DbQuery();
      $dbQuery->select('img.*,' .implode(',', $i18nSelect))
              ->from(self::$definition['table'], 'img');

      foreach(Language::getLanguages(false) as $lang) {
        $dbQuery->leftJoin(self::$definition['table'].'_lang', 
        'i18n_'.$lang['id_lang'], 
        'img.id_bbgallery_image = i18n_'.$lang['id_lang'].'.id_bbgallery_image'.
        ' AND i18n_'.$lang['id_lang'].'.id_lang = '.$lang['id_lang']
        );
      }

      $dbQuery->where('id_bbgallery_gallery = '.(int) $galleryId)
              //->where('i18n_default.id_lang = '. $defaultLanguage->id)
              ->groupBy('img.id_bbgallery_image')
              ->orderBy('position ASC');

      $galleryImages = Db::getInstance(_PS_USE_SQL_SLAVE_)
                ->executeS($dbQuery);

      $galleryImages = self::addImageThumbnailsToList($galleryImages, $alwaysRegenerateThumbnails, $disableCache);      

      return $galleryImages;
  }

  /**
   * 
   */
  public static function updatePositions($imageIds) 
  {
    foreach ($imageIds as $position => $id_image) {
      Db::getInstance()
        ->update(
            self::$definition['table'], 
            array('position' => (int)$position * 10), 
            'id_bbgallery_image = '.(int)$id_image 
          );
    }
  }

  /**
   * 
   */
  private static function addImageThumbnailsToList($images, $alwaysRegenerateThumbnails = true, $disableCache = true)
  {
    $clonedImages = $images;
    array_walk($clonedImages, function($val, $key) use(&$images, $alwaysRegenerateThumbnails, $disableCache) {
      $images[$key]['thumbnails'] = self::getAllThumbnails(
        $val['id_bbgallery_image'], 
        $val['image'],
        $alwaysRegenerateThumbnails,
        $disableCache
      );
    });

    return $images;
  }

  /**
   * @param int $id_bbgallery_image Id of the bbgallery_image for which to get the thumbnails
   * @param string $image Relative path of the full image
   * 
   * @return array
   */
  private static function getAllThumbnails($id_bbgallery_image, $image, $alwaysRegenerateThumbnails = true, $disableCache = true) 
  {
    return self::generateThumbnails(
      BarebonesGallery::IMAGES_DIRECTORY, 
      false,
      $image,
      $id_bbgallery_image,
      $alwaysRegenerateThumbnails,
      $disableCache
    );
  }

  /**
   * Generate slugs for the image at hand, for each language
   */
  private function populateSlug() 
  {
    // At this point in time: the slug will be made of the id, plain and simple
    // Any improvements on that, especially for SEO, can be made here in the future
    $url = $this->url;
    // Loop over every linguistic variation of the url, generate a slug for that
    array_walk($this->url, function($val, $index) use (&$url) {
        if (!is_array($url)) 
        {
          $url = array();
        }
        $url[$index] = $this->slugify($this->id);
    });
    $this->url = $url;
  }

  /**
   * Returns a slug for a given label
   */
  private function slugify($label) 
  {
    // For now, just a simple concatenation
    return 'image-'.$this->id;
  }

  private function getMaxPosition() 
  {
    $dbQuery = new DbQuery();
    $dbQuery->select('max(position) as max')
            ->from(self::$definition['table'], 'img')
            ->where('id_bbgallery_gallery = ' . (int)$this->id_bbgallery_gallery)
            ->limit('1');

    $maxQuery = Db::getInstance(_PS_USE_SQL_SLAVE_)
              ->executeS($dbQuery);

    if (!empty($maxQuery) && isset($maxQuery[0])) 
    {
      return (int)$maxQuery[0]['max'];
    } else 
    {
      return 0;
    }
  }

  /**
   * Defines the sort order for the image, by increments of 10
   * Lower means the image will be on top, the higher the number the lower it'll be
   */
  private function setPosition() 
  {
    $this->position = $this->getMaxPosition() + 10;
  }

  /**
   * Called whenever a new gallery is added
   */
  public function add($auto_date = true, $null_values = false)
  {
    $addResult = parent::add($auto_date = true, $null_values = false);

    if ($addResult) {
      // The slug being made of an id, execute that only after 
      // having had one to get the last inserted id
      $this->setPosition();
      $this->populateSlug();
      $this->save();
    }

    return $addResult;
  }

  /**
   * Called whenever an update is applied to the gallery
   */
  public function update($null_values = false) 
  {
    $this->populateSlug();

    return parent::update($null_values);
  }

  /**
   * 
   */
  public function delete() 
  {
    $currentId = $this->id_bbgallery_image;
    $deleteStatus = parent::delete();

    self::deleteAllThumbnails($currentId);

    return $deleteStatus;
  }

  /**
   * 
   */
  public function validateField($field, $value, $id_lang = null, $skip = array(), $human_errors = false) 
  {
    $isEditingImage = Tools::isSubmit('editing_image');
    if ($isEditingImage && $field == 'image') 
    {
      // Problem: when validating images during edit, the input field for the image is empty
      // Hence, there would be a validation error.
      // Here, no short-term choice as to bypass image field validation on edit
      $skip = array('image', 'required'/*, 'validate'*/);
      if (empty($value)) 
      {
        $value = Tools::getValue('editing_image');
      }
    }
    return parent::validateField($field, $value, $id_lang, $skip, $human_errors);
  }

  /**
   * 
   */
  private static function deleteAllThumbnails($id_bbgallery_image) 
  {
    $thumbnailsPaths = self::getAllThumbnailsPaths($id_bbgallery_image);
    foreach($thumbnailsPaths as $thumbnails) {
      $thumbFullPath = _PS_ROOT_DIR_.$thumbnails;
      if (file_exists($thumbFullPath)) {
        unlink($thumbFullPath);
      }
    }
    //dump($thumbnailsPaths); exit();
  }

  /**
   * 
   */
  private static function getAllThumbnailsPaths($id_bbgallery_image) 
  {
    $thumbnailPaths = array();
    foreach(self::$thumbnailTypes as $type => $value) 
    {
      if ($value !== false) 
      {
        $thumbnailPaths[] = self::getThumbnailPath($type, $id_bbgallery_image);
      }
    }

    return $thumbnailPaths;
  }

  /**
   * 
   */
  private static function getThumbnailPath($type, $id_bbgallery_image) 
  {
    return ImageManager::getThumbnailPath(
      $type.'_'.self::$definition['table']
      .'_'
      .(int)$id_bbgallery_image
      .'.'.self::$preferedThumbnailExtension, 
     false);
  }

  /**
   * Generate a thumbnail for the image given a specific size and a base path
   */
  private static function generateThumbnail($basePath, $type, 
   $image, $id_bbgallery_image, $alwaysRegenerateThumbnails = true, $disableCache = true) 
  {
    if (empty($image) ||
      !in_array($type, array_keys(self::$thumbnailTypes)) || $type == 'original') { 
        // Do not generate anything if no image, no expected type, or 
        // is the original pic
      return false;
    }

    try {
      $imageUrl = ImageManager::thumbnail(
        $basePath.$image, 
        $type.'_'.self::$definition['table']
        .'_'
        .(int)$id_bbgallery_image
        .'.'.self::$preferedThumbnailExtension, 
        self::$thumbnailTypes[$type], 
      self::$preferedThumbnailExtension, 
      $disableCache, 
      $alwaysRegenerateThumbnails);

      return $imageUrl;
    } catch(Exception $e) {
      // Silent exception here, return false in case of error
      // TODO: implement/look into reusing existing (?) Prestashop logger instead
      return false;
    }
  }

  /**
   * @return string|array
   */
  public static function generateThumbnails($basePath, $thumbnailType = false,
    $image, $id_bbgallery_image, $alwaysRegenerateThumbnails = true, $disableCache = true) 
  {
    if ($thumbnailType !== false && 
      !in_array($thumbnailType, array_keys(self::$thumbnailTypes))) {
      return false;
    }

    if ($thumbnailType !== false) {
      return self::generateThumbnail($basePath, $thumbnailType, 
        $image, $id_bbgallery_image, $alwaysRegenerateThumbnails, $disableCache);
    } else {
      $generatedThumbnails = array();
      foreach(self::$thumbnailTypes as $thumbnailKey => $thumbnailSize) {
        $imgTag = self::generateThumbnail($basePath, $thumbnailKey, 
          $image, $id_bbgallery_image, $alwaysRegenerateThumbnails, $disableCache);
        if (false !== $imgTag) 
        {
          preg_match( '/src="([^"]*)"/i', $imgTag, $extractedSrc) ;
          $generatedThumbnails[$thumbnailKey] = 
            (!empty($extractedSrc) && isset($extractedSrc[1]))? 
              $extractedSrc[1]: false;
        }
      }

      return $generatedThumbnails;
    }
  }

  /**
   * 
   */
  public function getThumbnail($basePath, $type) 
  {
    if (in_array($type, array_keys(self::$thumbnailTypes))) {
      return $this->generateThumbnails($basePath, $type, 
        $this->image, $this->id_bbgallery_image);
    }

    return false;
  }

}