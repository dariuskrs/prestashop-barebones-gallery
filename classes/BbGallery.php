<?php

class BbGallery extends ObjectModel
{
    public $id_bbgallery_gallery;

    public $title;

    public $description;

    public $url;

    public $meta_title;

    public $meta_description;

    public $meta_keyword;

    // public $configuration;

    public $position;

    public $active;

    public $date_add;

    public static $definition = [
      'table' => 'bbgallery_gallery',
      'primary' => 'id_bbgallery_gallery',
      'multilang' => true,
      'fields' => [
        'title' =>              ['type' => self::TYPE_STRING, 'validate' => 'isAnything', 'required'=>true, 'lang' => true],
        'description' =>        ['type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'lang' => true],
        'url' =>                ['type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true],
        'meta_title' =>         ['type' => self::TYPE_STRING, 'validate' => 'isAnything', 'lang' => true],
        'meta_description' =>   ['type' => self::TYPE_STRING, 'validate' => 'isAnything', 'lang' => true],
        'meta_keyword' =>       ['type' => self::TYPE_STRING, 'validate' => 'isAnything', 'lang' => true],
        'position' =>           ['type' => self::TYPE_INT, 'validate' => 'isunsignedInt'],
        'active' =>             ['type' => self::TYPE_BOOL, 'validate' => 'isBool'],
        'date_add' =>           ['type' => self::TYPE_DATE, 'validate'  => 'isDate', 'copy_post' => false]
        //'created' =>          ['type' => self::TYPE_DATE, 'validate' => 'isDateFormat'],
      ],
    ];

    private $slugCache = null;

    /**
     * 
     */
	public function __construct($id = null, $id_lang = null, $id_gallery_shop = null)
	{
        // One gallery is always associated to one given shop
        Shop::addTableAssociation('bbgallery_gallery', array('type' => 'shop'));
        if (empty($id_gallery_shop)) 
        {
            $id_gallery_shop = (int)Context::getContext()->shop->id;
        }
        parent::__construct($id, $id_lang, $id_gallery_shop);
  }

  /**
   * Populates the slug for the gallery if and only if no slug has been given
   */
  public function populateSlug() 
  {
    $title = $this->title;
    $url = $this->url;
    // Loop over every linguistic variation of the url, generate a slug for that
    array_walk($this->url, function($val, $index) use ($title, &$url) {
      if (/*empty($val) &&*/ is_array($title) && isset($title[$index])) {
        if (!is_array($url)) 
        {
          $url = array();
        }
        $titleToSlugify = $title[$index];
        // Problem: what if in any other language than the default one there is
        // currently no title (is empty)? Then, generate a slug from the default language
        if (empty($titleToSlugify)) 
        {
          $defaultLanguage = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
          if (isset($title[$defaultLanguage->id])) 
          {
            $titleToSlugify = $title[$defaultLanguage->id];
          }
        }
        $url[$index] = $this->generateSlug($titleToSlugify, $index);
      }
    });
    $this->url = $url;
  }

  /**
   * Get all currently used gallery slugs, sorted by language
   */
  private function getAllSlugs($ignoreGalleryId = null) 
  {
    $dbQuery = new DbQuery();
    $dbQuery->select('i18n.id_bbgallery_gallery, i18n.id_lang, i18n.url')
            ->from(self::$definition['table'], 'gal')
            ->leftJoin(self::$definition['table'].'_lang', 
              'i18n', 
              'gal.id_bbgallery_gallery = i18n.id_bbgallery_gallery'
            );

    if (!empty($ignoreGalleryId)) 
    {
      $dbQuery->where('i18n.id_bbgallery_gallery != '. $ignoreGalleryId);
    }

    $dbQuery->orderBy('i18n.id_bbgallery_gallery')
            ->groupBy('i18n.id_bbgallery_gallery, i18n.id_lang');

    return Db::getInstance(_PS_USE_SQL_SLAVE_)
      ->executeS($dbQuery);
  }

  /**
   * 
   */
  private function getSlugListByLanguage($ignoreGalleryId = null) 
  {
    $allSlugs = $this->getAllSlugs($ignoreGalleryId);

    $allLanguages = Language::getLanguages(false, (int)Context::getContext()->shop->id, true);
    $slugList = array();
    foreach($allLanguages as $langIndex => $langKey) {
      $slugList[$langKey] = array();
    }

    foreach($allSlugs as $existingSlug) {
      if (isset($existingSlug['id_lang']) && !empty($existingSlug['url'])) 
      {
        $langKey = $existingSlug['id_lang'];
        $existingUrl = $existingSlug['url'];
        if (!in_array($existingUrl, $slugList[$langKey])) 
        {
          $slugList[$langKey][] = $existingUrl;
        }
      }
    }

    return $slugList;
  }

  /**
   * 
   */
  private function generateSlug($rawTitle, $languageCode) 
  {
    $ignoreGalleryId = null;
    if (!empty($this->id_bbgallery_gallery)) 
    {
      $ignoreGalleryId = $this->id_bbgallery_gallery;
    }
    $slugList = $this->getSlugListByLanguage($ignoreGalleryId);
    $slugCandidate = $this->slugify($rawTitle);

    if (isset($slugList[$languageCode])) 
    {
      $slugCandidate = $this->generateUniqueSlug($slugCandidate, $slugList[$languageCode]);
    }

    return $slugCandidate;
  }

  /**
   * 
   * @return array|bool An array of gallery attributes
   */
  public static function getBySlug($slug, $onlyForGivenLocale = false) 
  {
    if (empty($slug)) {
      return false;
    }

    $dbQuery = new DbQuery();
    $dbQuery->select('*')
            ->from(self::$definition['table'], 'gal')
            ->leftJoin(self::$definition['table'].'_lang', 
              'i18n', 
              'gal.id_bbgallery_gallery = i18n.id_bbgallery_gallery'
            )
            ->limit(1);

    if ($onlyForGivenLocale) 
    {
      $dbQuery->where('i18n.id_lang = '. (int)$onlyForGivenLocale);
    }

    $dbQuery->where('i18n.url = "'.$slug.'"');

    $result = Db::getInstance()->executeS($dbQuery); 

    if (empty($result) || !array($result)) {
      return false;
    }

    return $result[0];
  }

  /**
   * 
   */
  private function generateUniqueSlug($slugCandidate, $existingSlugs, $iterator = '') 
  {
    if (in_array($slugCandidate, $existingSlugs)) 
    {
      if (empty($iterator)) 
      {
        $iterator = 1;
      }
      $stringSuffix = strval($iterator);
      $slugCandidate = $this->generateUniqueSlug($slugCandidate . $stringSuffix, $existingSlugs, $iterator++);
    }

    return $slugCandidate;
  }

  /**
   * 
   */
  private function slugify($string, $delimiter = '-') 
  {
    $oldLocale = setlocale(LC_ALL, '0');
    setlocale(LC_ALL, 'en_US.UTF-8');
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower($clean);
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    $clean = trim($clean, $delimiter);
    setlocale(LC_ALL, $oldLocale);
    return $clean;
  }

  /**
   * Called whenever a new gallery is added
   */
  public function add($auto_date = true, $null_values = false)
  {
    $this->populateSlug();

    return parent::add($auto_date = true, $null_values = false);
  }

  /**
   * Called whenever an update is applied to the gallery
   */
  public function update($null_values = false) 
  {
    $this->populateSlug();

    return parent::update($null_values);
  }


}