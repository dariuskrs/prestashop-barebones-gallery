<?php
require_once _PS_MODULE_DIR_.'/barebonesgallery/classes/BbGallery.php';
require_once _PS_MODULE_DIR_.'/barebonesgallery/classes/BbImage.php';

/**
 *
 */
class AdminBarebonesGalleryMgmtGalleryController extends ModuleAdminController
{
    /**
     *
     */
    public function __construct()
    {
        //error_reporting(E_ALL & ~E_NOTICE);
        parent::__construct();

        $this->bootstrap = true;

        if ($this->formHasBeenSubmitted()) {
            if (Tools::isSubmit('submitAddbbgallery_gallery')
            || Tools::isSubmit('submitFilterbbgallery_gallery')) {
                $this->initGalleryList();
            } else if (Tools::isSubmit('submitAddbbgallery_image')) {
                $this->configureImageAddEditForm();
            }

        } else {
            if (Tools::isSubmit('manageimages')) {
                $this->display = 'manageimages';
            } else if(Tools::isSubmit('addimage')) {
                $this->display = 'addimage';
            } else if(Tools::isSubmit('updateimage')) {
                $this->display = 'updateimage';
            }  else if(Tools::isSubmit('deleteimage')) {
                $this->display = 'deleteimage';
            } else if(Tools::isSubmit('deletebbgallery_image')) {
                $this->initGalleryImageDeletion();
            }
            else {
                $this->initGalleryList();
            }
        }
    }

    /**
     * 
     */
    private function initGalleryImageDeletion() 
    {
        $this->table = 'bbgallery_image';
        $this->identifier = 'id_bbgallery_image';
        $this->className = 'BbImage';
        $this->object = new BbImage(Tools::getValue('id_bbgallery_image', null));
    }

    /**
     * 
     */
    public function init()
    {
        parent::init();
        //$this->initGalleryList();
        /*
        if (Tools::isSubmit('submitAddbbgallery_image')) {
            $this->display = 'addimage';
        } else if (Tools::isSubmit('manageimages')) {
            $this->display = 'manageimages';
        } else if(Tools::isSubmit('addimage')) {
            $this->display = 'addimage';
        }
        */
    }

    /**
     * @throws Exception
     * @throws SmartyException
     */
    public function initContent()
    {
        if ($this->display == 'edit' || $this->display == 'add') {
            if (Tools::isSubmit('submitAddbbgallery_image')) {
                $this->display = 'addimage';
                $this->renderImageAddEditForm();
            } else {
                $this->defineCreateEditForm();
            }
        } else if ($this->display == 'manageimages') {
            return $this->renderImageManagementForm();
        } else if (in_array($this->display, array('addimage', 'updateimage'))) {
            return $this->renderImageAddEditForm();
        } 

        parent::initContent();
    }

    /**
     * @throws Exception
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function initProcess()
    {
        parent::initProcess();
    }

    /**
     * Check whether a form has been submitted
     * By accepted standard within prestashop and this module, all of the 
     * submitted form register a "submitXXXX" key in their POST
     */
    private function formHasBeenSubmitted() 
    {
        return !empty(
            array_filter($_POST, function($k) {
                return strpos($k, 'submit') === 0;
            }, ARRAY_FILTER_USE_KEY)
        );
    }

    /**
     *
     */
    private function initGalleryList()
    {
        // ini_set('display_errors', 'on');
        $this->bootstrap = true; // use Bootstrap CSS
        $this->table = 'bbgallery_gallery'; // SQL table name, will be prefixed with _DB_PREFIX_
        $this->identifier = 'id_bbgallery_gallery'; // SQL column to be used as primary key
        $this->lang = true;
        $this->explicitSelect = true;
        $this->deleted = false;
        $this->className = 'BbGallery'; // PHP class name
        $this->allow_export = false; // allow export in CSV, XLS..
        //$this->module = 'barebonesgallery';
        $this->_defaultOrderBy = 'position'; // the table alias is always `a`
        $this->_defaultOrderWay = 'ASC';
        if(Shop::isFeatureActive())
        {
            Shop::addTableAssociation($this->table,
                array('type' => 'shop')
            );
        }
        $this->fields_list = [
            'id_bbgallery_gallery' => [
                'title' => $this->l('Id'),
                'class' => 'fixed-width-xs'
            ],
            'title' => [
                'title' => $this->l('Title')
            ],
            'description' => [
                'title' => $this->l('Description')
            ],
            'url' => [
                'title' => $this->l('Slug' ),
                'width' => 220,
                'type' => 'text'
            ],
            /*
            'position' => [
	            'title' => $this->l('Position' ),
				'align' => 'left',
				'position' => 'position',
            ],
            */
            'active' => [
                'title' => $this->l('Status' ),
                'width' => 60,
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false
            ]
        ];

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );

        // Create/Edit/Detail
        $this->addRowAction('manageimages');
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        //$this->addRowAction('details');
    }

    /**
     *
     */
    private function defineCreateEditForm()
    {
        $this->fields_form = [
            'legend' => [
              'title' => 
                    (empty($this->object)) ? 
                    $this->l('Add new gallery'):
                    $this->l('Edit gallery')
                ,
              'icon' => 'icon-list-ul'
            ],
            'input' => [
              [
                  'name' => 'title',
                  'type' => 'text',
                  'label' => $this->l('Title'),
                  'required' => true,
                  'lang' => true
              ],
              [
                  'name' => 'description',
                  'type' => 'textarea',
                  'label' => $this->l('Description'),
                  'lang' => true
              ],
              [
                'name' => 'meta_title',
                'type' => 'text',
                'label' => $this->l('Meta Title'),
                'required' => false,
                'lang' => true
            ],
              [
                'name' => 'meta_description',
                'type' => 'textarea',
                'label' => $this->l('Meta description'),
                'required' => false,
                'lang' => true
              ],
              [
                'name' => 'meta_keyword',
                'type' => 'textarea',
                'label' => $this->l('Meta keywords'),
                'required' => false,
                'lang' => true
              ],
              [
                  'name' => 'url',
                  'type' => 'text',
                  'label' => $this->l('Slug' ),
                  'lang' => true,
                  //'hint' => $this->l('The value which appears in the gallery URL. Will be generated automatically, unless a different value is specified.'),
                  'required'  => false,
                  'desc' => $this->l('Address of the image; will be generated automatically')
              ],
              [
                  'name' => 'active',
                  'type' => 'hidden', // should be switch
                  'default_value' => 1,
                  'label' => $this->l('Active' ),
                  'is_bool' => true,
                  'values' => array(
                      array(
                          'id' => 'active',
                          'value' => 1,
                          'label' => $this->l('Enabled' )
                      ),
                      array(
                          'id' => 'active',
                          'value' => 0,
                          'label' => $this->l('Disabled' )
                      )
                  )
              ]
            ],
            'submit' => [
              'title' => $this->l('Save'),
            ]
          ];

          if(Shop::isFeatureActive())
          {
              $this->fields_form['input'][] = [
                  'type' => 'shop',
                  'label' => $this->l('Shop association:' ),
                  'name' => 'checkBoxShopAsso',
              ];
          }
    }

    /**
     *
     */
    private function renderImageManagementForm()
    {
        $this->fields_form = [
            'legend' => [
              'title' => $this->l('Manage images'),
              'icon' => 'icon-list-ul'
            ],
            'input' => [
            ],
            'submit' => [
              'title' => $this->l('Save'),
            ]
          ];

        if(Shop::isFeatureActive())
        {
            $this->fields_form['input'][] = [
                'type' => 'shop',
                'label' => $this->l('Shop association:' ),
                'name' => 'checkBoxShopAsso',
            ];
        }

        $imageList = BbImage::getImagesByGalleryId(
            Tools::getValue('id_bbgallery_gallery', null));
            //dump($imageList);exit();
        
        $this->context->smarty->assign(array(
            'link' => $this->context->link,
            'id_bbgallery_gallery' => Tools::getValue('id_bbgallery_gallery'),
            'languages' => Language::getLanguages(false),
            'defaultLanguage' => Configuration::get('PS_LANG_DEFAULT'),
            'images' => $imageList,
            'image_baseurl' => Barebonesgallery::IMAGES_DIRECTORY, 
        ));

        $this->content = $this->context
            ->smarty->createTemplate(_PS_MODULE_DIR_ .
                $this->module->name .'/views/admin/list_images.tpl')
            ->fetch();

        return $this->context->smarty->assign(array(
            'content' => $this->content,
        ));
    }

    /**
     * 
     */
    public function postProcess() 
    {
        parent::postProcess();
    }

    /**
     * 
     */
    private function configureImageAddEditForm() 
    {
        $this->table = 'bbgallery_image';
        $this->className = 'BbImage';
        $this->identifier = 'id_bbgallery_image'; // SQL column to be used as primary key
        $this->lang = true;
        $this->display == 'addimage';
        $this->_defaultOrderBy = 'position';
        $this->_defaultOrderWay = 'ASC';

        /*
        $idGalleryImage = Tools::getValue('id_bbgallery_image', null);
        if (null != $idGalleryImage) {
            $this->setIdObject((int)$idGalleryImage);
        }
        */
    }

    /**
     * 
     */
    private function renderImageAddEditForm()
    {
        // Define which table to query
        $this->configureImageAddEditForm();
        $idGalleryImage = Tools::getValue('id_bbgallery_image', null);
        $idGallery = Tools::getValue('id_bbgallery_gallery', null);

        // Create image object
        $image = new BbImage($idGalleryImage);
        if (null == $idGalleryImage) 
        {
            $image->setGalleryId($idGallery);
        }

        if (!empty($image) && file_exists(Barebonesgallery::IMAGES_DIRECTORY.$image->image)) 
        {
            $imageUrl = $image->getThumbnail(Barebonesgallery::IMAGES_DIRECTORY, 'small');
        }
        
        $form = [
            'legend' => [
              'title' => (empty($image) || $image->id_bbgallery_image == null) ? 
                $this->l('Add image'):
                $this->l('Edit image'),
              'icon' => 'icon-list-ul'
            ],
            'input' => [
               [
                'name' => 'id_bbgallery_image',
                'type' => 'hidden',
                'label' => $this->l('Gallery Img'),
                'required' => false,
              ], 
              [
                'name' => 'id_bbgallery_gallery',
                'type' => 'hidden',
                'label' => $this->l('Gallery'),
                'required' => true,
              ], 
              [
                'name' => 'image',
                //'type' => 'file_lang',
                'type' => 'file',
                'label' => $this->l('Image' ),
                'lang' => false, //////
                'required' => true,
                'display_image' => true,
                'image' => !empty($imageUrl) ? $imageUrl : false,
                'desc' => $this->l('Maximum image size: ') .' '. ini_get('upload_max_filesize')
              ],
              /*
              [
                  'name' => 'title',
                  'type' => 'text',
                  'label' => $this->l('Title'),
                  'required' => true,
                  'lang' => true
              ],
              [
                  'name' => 'legend',
                  'type' => 'textarea',
                  'label' => $this->l('Legend'),
                  'lang' => true
              ],
              */
              [
                  'name' => 'description',
                  'type' => 'textarea',
                  'label' => $this->l('Description'),
                  'lang' => true
              ],
              [
                  'name' => 'url',
                  'type' => 'text',
                  'label' => $this->l('Slug' ),
                  'lang' => true,
                  'desc' => $this->l('Address of the image; will be generated automatically')
              ],
              [
                  'name' => 'active',
                  'type' => 'hidden', // to be replaced by switch, whenever that needs to be allow for the admin
                  'label' => $this->l('Active' ),
                  'is_bool' => true,
                  'default_value' => 1,
                  'values' => array(
                      array(
                          'id' => 'active',
                          'value' => 1,
                          'label' => $this->l('Enabled' )
                      ),
                      array(
                          'id' => 'inactive',
                          'value' => 0,
                          'label' => $this->l('Disabled' )
                      )
                  )
              ]
            ],
            'submit' => [
              'title' => $this->l('Save'),
            ]
        ];

        // To replace when proper docs for 1.7.x + are out and we are allowed to go full symfony
        if (!empty($image) && !empty($image->image)) {
            $form['input'][] = array(
                'type' => 'hidden', 
                'name' => 'editing_image',
                'default_value' => $image->image
            );
        }

        $helper = new HelperForm();
        $helper->currentIndex = self::$currentIndex;
        $helper->token = $this->token;
        $helper->table = $this->table;
        $this->fields_form[0]['form'] = $form;
        $this->getlanguages();
        $helper->languages = $this->_languages;
        $helper->show_cancel_button = true;
        $helper->default_form_language = $this->context->language->id;
        //$helper->title = $this->l('Add image');
        $helper->fields_value = $this->getFieldsValue($image);

        $helper->submit_action = 'submitAdd'.$this->table;

        $this->content .= $helper->generateForm($this->fields_form);

        $this->context->smarty->assign(array(
            'content' => $this->content,
        ));
    }


    /**
     * Custom action: manage images
     */
    public function displayManageimagesLink($token, $id, $name = null)
    {
        $tpl =$this->context->smarty->createTemplate(
            'module:barebonesgallery/views/admin/list_action_manageimages.tpl');

        $tpl->assign(array(
            'href' => self::$currentIndex .
                '&' . $this->identifier . '=' . $id .
                '&manageimages&token=' . ($token != null ? $token : $this->token),
            'action' => $this->l('Manage images')
        ));

        return $tpl->fetch();
    }

    /**
     * 
     */
    public function beforeAdd($object) 
    {
        if (Tools::isSubmit('submitAddbbgallery_gallery') 
            && $this->object instanceOf BbGallery) 
        {
            // Upon adding 
            //$this->object->populateSlug();
        }

        return parent::beforeAdd($object);
    }

    /**
     * 
     */
    public function afterUpdate($object) 
    {
        $afterUpdateStatus = parent::afterUpdate($object);

        if (Tools::isSubmit('submitAddbbgallery_image') && empty($this->errors)) {
            // Process the image upload
            $this->processImages($object);
        }

        return $afterUpdateStatus;
    }

    /**
     * 
     */
    public function afterAdd($object) 
    {
        $afterAddStatus = parent::afterAdd($object);

        if (Tools::isSubmit('submitAddbbgallery_image') && empty($this->errors)) {
            // Process the image upload
            $this->processImages($object);
        }

        return $afterAddStatus;
    }

    /**
     * 
     */
    public function processSave()
    {
        // Save changes to the db, validate
        $saveStatus = parent::processSave();

        // No error + new/edit of image? Redirect to the overview of the images contained in the gallery
        if (Tools::isSubmit('submitAddbbgallery_image') && empty($this->errors)) {
            // Redirect to the image overview
            $this->redirect_after = $this->getLinkToGallery(Tools::getValue('id_bbgallery_gallery', null), 3);
        }

        return $saveStatus;
    }

    /**
     * 
     */
    private function processImages($object) 
    {
        if(Validate::isLoadedObject($object) && $object instanceof BbImage) 
        {
            if (!empty($_FILES['image']) && !empty($_FILES['image']['name'])) 
            {
                $newName = "{$object->id}-".$_FILES['image']['name'];
                $uploader = new \Uploader('image');
                $uploader->setSavePath(Barebonesgallery::IMAGES_DIRECTORY);
                $files = $uploader->process($newName);
                $object->image = $newName;
                BbImage::generateThumbnails(
                    Barebonesgallery::IMAGES_DIRECTORY,
                    false,
                    $object->image,
                    $object->id
                );
                $object->save();
            } else if (Tools::isSubmit('editing_image')) 
            {
                $object->image = Tools::getValue('editing_image');
                $object->save();
            }
        }
    }

    /**
     * 
     */
    public function processDelete()
    {
        $deleteStatus = parent::processDelete();

        if (Tools::isSubmit('deletebbgallery_image')) 
        {
            $this->redirect_after = $this->getLinkToGallery(Tools::getValue('id_bbgallery_gallery', null), 1);
        }

        return $deleteStatus;
    }

    /**
     * 
     */
    private function deleteImage($redirect = true) 
    {
        /*
        $slide = new Ps_HomeSlide((int)Tools::getValue('delete_id_slide'));
        $res = $slide->delete();
        $this->clearCache();
        if (!$res) {
            $this->_html .= $this->displayError('Could not delete.');
        } else {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&conf=1&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name);
        }
        */
        Tools::redirectAdmin($this->getLinkToGallery(Tools::getValue('id_bbgallery_gallery', null)), 3);
    }

    /**
     * 
     */
    private function getLinkToGallery($bbgalleryId, $confirmationLevel = 0) 
    {
        $controllerRootUrl = Context::getContext()->link->getAdminLink('AdminBarebonesGalleryMgmtGallery', true);
        return $controllerRootUrl .
            '&' . 'id_bbgallery_gallery' . '=' . 
            $bbgalleryId .
            '&conf=' . $confirmationLevel .
            '&manageimages' .
            '&token=' . $this->token;
    }

    /**
     * 
     */
    public function actionAdminBarebonesGalleryMgmtGalleryControllerSaveAfter() 
    {
        die("captain hook");
    }

    /**
     * 
     */
    public function hookAdminBarebonesGalleryMgmtGalleryControllerSaveAfter() 
    {
        die("captain hook");
    }

    /**
     * 
     */
    public function renderList()
    {
        return parent::renderList();
    }

}
