<?php
require_once _PS_MODULE_DIR_.'/barebonesgallery/classes/BbGallery.php';
require_once _PS_MODULE_DIR_.'/barebonesgallery/classes/BbImage.php';

/**
 *
 */
class BarebonesgalleryGalleryModuleFrontController extends ModuleFrontController
{
    private $moduleName = 'barebonesgallery';

    private $bbGallery = null;


    /**
     * @throws PrestaShopException
     */
    public function initContent()
    {
        parent::initContent();

        if (null == $this->bbGallery) {
            $this->redirectToHomepage();
        }

        $this->currentGalleryName = $this->bbGallery['title'];
        $imageList = BbImage::getImagesByGalleryId($this->bbGallery['id_bbgallery_gallery'], false, false);

        $this->context->smarty->assign(
            array(
                'gallery'               => $this->bbGallery,
                'imageList'             => $imageList,
                'currentLanguageId'     => $this->context->language->id
            )
        );
        
        $this->setTemplate('module:barebonesgallery/views/templates/front/gallery.tpl' /*, array(), $this->context->getCurrentLocale()*/);
    }

    /**
     * 
     */
    private function loadRequestedGallery() 
    {
        $gallerySlug = Tools::getValue('slug', null);

        if (null == $gallerySlug) {
            $this->redirectToHomepage();
        }

        // Get requested gallery
        $this->bbGallery = BbGallery::getBySlug($gallerySlug, $this->context->language->id);
    }

    /**
     * @return array
     */
    public function getTemplateVarPage()
    {
        $this->loadRequestedGallery();
        
        $page = parent::getTemplateVarPage();

        if (!empty($this->bbGallery)) {
            $page['meta']['title'] = (!empty($this->bbGallery['title'])) ? 
                Configuration::get('PS_SHOP_NAME') . ' - ' . $this->bbGallery['title'] :
                Configuration::get('PS_SHOP_NAME');
            $page['meta']['description'] = (!empty($this->bbGallery['meta_description'])) ? 
                $this->bbGallery['meta_description'] :
                $this->bbGallery['description'];
            if (!empty($this->bbGallery['meta_keyword'])) {
                $page['meta']['keywords'] = $this->bbGallery['meta_keyword'];
            }
        }

        return $page;
    }

    /**
     * 
     */
    private function redirectToHomepage() 
    {
        $this->redirect_after = $this->context->link->getPageLink('');
        $this->redirect();
    }

    /**
     * @return bool|void
     */
    public function setMedia()
    {
        $stateOfMediaInit = parent::setMedia();

        $this->context->controller->registerJavascript(
            'bbgallery-frontend-js', 
            'modules/'.$this->moduleName.'/views/js/builds/bbgallery.min.js', array('priority' => 150));

        $this->context->controller->registerStylesheet(
            'bbgallery-frontend-css', 
            'modules/'.$this->moduleName.'/views/css/builds/bbgallery.min.css', array('priority' => 150));

        return $stateOfMediaInit;
    }


}