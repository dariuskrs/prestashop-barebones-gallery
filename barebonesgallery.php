<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Adds a gallery and its own content type
 */
class Barebonesgallery extends Module
{
    public const IMAGES_DIRECTORY =  _PS_MODULE_DIR_.
    'barebonesgallery'. DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

    private const MODULE_ROOT_URL = 'gallery';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->name = 'barebonesgallery';
        $this->tab = 'content_management';
        $this->version = '0.2.0';
        $this->ps_versions_compliancy = array('min' => '1.7.6.0', 'max' => _PS_VERSION_);
        $this->author = 'Dariuskrs';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Barebones gallery');
        $this->description = $this->l('Adds a gallery page to prestashop.');
    }

    /**
     * Return the version of the module
     */
    public static function getModuleVersion() 
    {
        $module = Module::getInstanceByName('barebonesgallery'); 
        if (!empty($module)) {
            return $module->version;
        }

        return '0.0';
    }

    /**
     * 
     */
    public function install()
    {
        // Exception follows if this step does not pass
        $this->createModuleTables();

        // Register hooks and menu
        if (!parent::install()
            || !$this->registerHook('header')
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('displayBackOfficeHeader')
            || !$this->registerHook('displayAdminAfterHeader')
            || !$this->registerHook('moduleRoutes')
            || !$this->registerAdminMenu()
        ) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public function uninstall()
    {
        // Remove added tables
        $this->deleteModuleTables();

        return parent::uninstall();
    }

    /**
     * Opt-in for the new module translation system
     */
    /*
    nope: cannot generate i18n files / have to export from DB 
    => don't want to bother with that right now so let's use what 
    currently (1.7.6.x) works
    public function isUsingNewTranslationSystem()
    {
        return true;
    }
    */

    /**
     * 
     */
    public function hookModuleRoutes() 
    {
        //$baseUrl = Configuration::get(self::BASE_GALLERY_URL);
        return array(
            'barebones-gallery-module' => array(
                'controller' =>  'gallery',
                'rule' => $this->l('gallery').'/{slug}',// $this->l('gallery')
                'keywords' => array(
                    'slug' => array('regexp' => '[A-Za-z0-9-]+', 'param' => 'slug'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => $this->name,
                    /*
                    'subpage_type' => 'post',
                    'page_type' => 'category',
                    */
                )
            )
        );
    }

    /**
     *
     */
    public function hookDisplayBackOfficeHeader()
    {
        if ($this->context->controller != null) 
        {
            $this->context->controller->addCSS(
                $this->_path .'views/css/backoffice-styles.css');

            // Yup, I know. Don't have any solution at hand to include script type=modules as of now
            $this->context->controller->addJquery();
            $this->context->controller->addJqueryUI('ui.sortable');
            /*
            $this->context->controller->addJS(
                $this->_path .'node_modules/sortablejs/modular/sortable.complete.esm.js');
            */
            $this->context->controller->addJS(
                $this->_path .'views/js/backoffice-sortable.js');

            $this->context->controller->addJS(
                $this->_path .'views/js/backoffice-main.js');
        }
    }

    /**
     * 
     */
    public function hookDisplayHeader()
    {

    }

    /**
     * 
     */
	public function hookDisplayAdminAfterHeader()
	{
        if (Tools::getValue('controller') == 'AdminBarebonesGalleryMgmtGallery' &&
            !Tools::isSubmit('configure') && 
            !Tools::isSubmit('manageimages') &&
            !Tools::isSubmit('addbbgallery_gallery') &&
            !Tools::isSubmit('id_bbgallery_gallery') &&
            !Tools::isSubmit('id_bbgallery_image')) {

            $languageSpecificGalleryPaths = array();
            $languages = Language::getLanguages();
            foreach($languages as $lang) {
                $languageSpecificGalleryPaths[] = array(
                    'lang' => $lang['name'],
                    'url' => _PS_BASE_URL_.'/'
                        .$lang['iso_code'].'/'
                        .$this->l(self::MODULE_ROOT_URL, false, $lang['iso_code'])
                );
            }
            $this->context->smarty->assign(array('gallery_paths' => $languageSpecificGalleryPaths));
            return $this->display(__FILE__, 'views/admin/helper_message.tpl');
        }
    }

    /**
     * Registers the needed menus for the module
     */
    private function registerAdminMenu() 
    {
        $menuItems = $this->getMenu();
        $registerMenuSuccess = true;
        foreach($menuItems as $menuItem) {
            if (!$this->installMenuTab($menuItem)) 
            {
                $registerMenuSuccess = false;
            }
        }

        return $registerMenuSuccess;
    }

    /**
     * 
     */
    public function getMenu()
    {
        return array(
            // Main menu
            array(
                'class_name' => 'AdminBarebonesGallery',
                'name' => 'Barebones Gallery',
                'parent_class_name' => 'IMPROVE',
                'icon' => 'collections'
            ),
            // Sub menus
            array(
                'class_name' => 'AdminBarebonesGalleryMgmtGallery',
                'name' => $this->l('Manage Galleries'),
                'parent_class_name' => 'AdminBarebonesGallery'
            )
            );
    }

    /**
     * @return bool|int False if menu tab creation failed, an id if the menu tab was inserted correctly
     */
    private function installMenuTab($menuItem) 
    {
        // Validate inputs
        if (empty($menuItem)) 
        {
            return false;
        }

        $mandatoryKeys = array('class_name', 'parent_class_name', 'name');
        foreach($mandatoryKeys as $key) 
        {
            if (!array_key_exists($key, $menuItem)) 
            {
                return false;
            }
        }

        // Create the menu tab with the passed information
        $languages = Language::getLanguages();
        $subMenuItemTab = new Tab();
        $subMenuItemTab->class_name = $menuItem['class_name'];
        (!empty($menuItem['icon']))? $subMenuItemTab->icon = $menuItem['icon'] : true;
        $subMenuItemTab->id_parent = (int) Tab::getIdFromClassName($menuItem['parent_class_name']);
        $subMenuItemTab->module = $this->name;
        foreach($languages as $language)
        {
            $subMenuItemTab->name[$language['id_lang']] = $this->l($menuItem['name']);
        }

        return $subMenuItemTab->save();
    }

    /**
     * @throws Exception
     */
    private function createModuleTables()
    {
        // Contains key->value pairs for the module config - as of now, not used (yet)
        $gallerySettings = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."bbgallery_settings` (
            `id_bbgallery_settings` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `setting_key` VARCHAR(150) NOT NULL ,
            `setting_value` TEXT NOT NULL,
            PRIMARY KEY (`id_bbgallery_settings`)
        )";

        // Contains galleries. Galleries are abstractions containing images
        $gallery = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."bbgallery_gallery` (
            `id_bbgallery_gallery` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `position` INT(10) UNSIGNED NOT NULL DEFAULT '0',
            `configuration` TEXT NULL,
            `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
            `id_cover_image` INT(10) UNSIGNED NULL,
            `date_add` DATETIME NOT NULL,
            PRIMARY KEY (`id_bbgallery_gallery`)
        )";

        // Gallery i18n metadata: title, description, metas (SEO), url slug
        $galleryLang = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."bbgallery_gallery_lang` (
            `id_bbgallery_gallery` INT(10) UNSIGNED NOT NULL,
            `id_lang` int(10) unsigned NOT NULL,
            `title` text NOT NULL,
            `description` longtext NOT NULL DEFAULT '',
            `category` VARCHAR(100) NOT NULL DEFAULT '',
            `meta_title` varchar(300) NOT NULL DEFAULT '',
            `meta_description` longtext NOT NULL DEFAULT '',
            `meta_keyword` longtext NOT NULL DEFAULT '',
            `url` varchar(400) NOT NULL DEFAULT '',
            PRIMARY KEY (`id_bbgallery_gallery`, `id_lang`)
        )";

        // Gallery shop table, to support multi shop installs of prestashop (to be tested, should work in theory)
        $galleryShop = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."bbgallery_gallery_shop` (
            `id_bbgallery_gallery` INT(10) UNSIGNED NOT NULL,
            `id_shop` INT(10) UNSIGNED NOT NULL,
            PRIMARY KEY (`id_bbgallery_gallery`, `id_shop`)
        )";

        // Images contained in galleries
        $image = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."bbgallery_image` (
            `id_bbgallery_image` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_bbgallery_gallery` INT(10) UNSIGNED NOT NULL,
            `image` varchar(255) NOT NULL,
            `position` int(10) unsigned NOT NULL DEFAULT '0',
            `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
            `date_add` DATETIME NOT NULL,
            PRIMARY KEY (`id_bbgallery_image`),
            INDEX `id_bbgallery_gallery` (`id_bbgallery_gallery`)
        )";

        // i18n metadata for images: title, description, url slug
        // NB: each of those are translatable, but the 
        // actual link to the image is shared accross all languages (see bbgallery_image)
        $imageLang = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."bbgallery_image_lang` (
            `id_bbgallery_image` INT(10) UNSIGNED NOT NULL,
            `id_lang` int(10) unsigned NOT NULL,
            `title` varchar(255) NOT NULL DEFAULT '',
            `description` text NOT NULL DEFAULT '',
            `legend` varchar(255) NOT NULL DEFAULT '',
            `url` varchar(255) NOT NULL,
            PRIMARY KEY (`id_bbgallery_image`, `id_lang`)
        )";

        $createTablesQueries = array(
            'gallery_settings'  => $gallerySettings,
            'gallery'           => $gallery,
            'gallery_lang'      => $galleryLang,
            'gallery_shop'      => $galleryShop,
            'image'             => $image,
            'image_lang'        => $imageLang
        );

        try {
            foreach ($createTablesQueries as $name => $createTablesQuery) 
            {
                Db::getInstance()
                  ->execute($createTablesQuery);
                //$this->logger->info('Table '.$name.' created.');
            }
        } catch (Exception $e) 
        {
            //$this->logger->error($e->getMessage());
            throw new Exception($this->l('Cannot create tables.'));
        }
    }

    private function deleteModuleTables() 
    {
        $gallerySettings = "DROP TABLE IF EXISTS `"._DB_PREFIX_."bbgallery_settings`;";
        $gallery = "DROP TABLE IF EXISTS `"._DB_PREFIX_."bbgallery_gallery`;";
        $galleryLang = "DROP TABLE IF EXISTS `"._DB_PREFIX_."bbgallery_gallery_lang`;";
        $galleryShop = "DROP TABLE IF EXISTS `"._DB_PREFIX_."bbgallery_gallery_shop`;";
        $image = "DROP TABLE IF EXISTS `"._DB_PREFIX_."bbgallery_image`;";
        $imageLang = "DROP TABLE IF EXISTS `"._DB_PREFIX_."bbgallery_image_lang`;";

        $deleteTablesQueries = array(
            'gallery_settings'  => $gallerySettings,
            'gallery'           => $gallery,
            'gallery_lang'      => $galleryLang,
            'gallery_shop'      => $galleryShop,
            'image'             => $image,
            'image_lang'        => $imageLang
        );

        try {
            foreach ($deleteTablesQueries as $name => $deleteTablesQuery) 
            {
                Db::getInstance()
                  ->execute($deleteTablesQuery);
            }
        } catch (Exception $e) 
        {
            throw new Exception($this->l('Cannot drop tables.'));
        }
    }

    /**
     * Hook into the header info
     */
    /*
    public function hookDisplayHeader(array $params)
    {
        if ($this->context->controller != null) 
            {
                return $this->display(__FILE__, 'views/headers.tpl');
            }

    }
    */


}
